package com.chen.service.impl;

import com.chen.dao.DeptDao;
import com.chen.entity.Dept;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.chen.service.DeptService;

import java.util.List;

@Service
public class DeptServiceimpl implements DeptService {

    @Autowired
    private DeptDao dao;

    @Override
    public boolean add(Dept dept) {
        return dao.addDept(dept);
    }

    @Override
    public Dept get(Long id) {
        return  dao.findById(id);
    }

    @Override
    public List<Dept> list() {
        return dao.findAll();
    }
}
